import { AppDataSource } from "./data-source";
import { Role } from "./entity/Role";
import { User } from "./entity/User";

AppDataSource.initialize()
  .then(async () => {
    const usersRepository = AppDataSource.getRepository(User);
    await usersRepository.clear();
    const rolesRepository = AppDataSource.getRepository(Role);
    await rolesRepository.clear();
    console.log("Inserting a new role into the Memory...");
    var role = new Role();
    role.id = 1;
    role.name = "admin";

    console.log("Inserting a new role into the database...");
    await rolesRepository.save(role);

    role = new Role();
    role.id = 2;
    role.name = "user";

    console.log("Inserting a new role into the database...");
    await rolesRepository.save(role);

    const roles = await rolesRepository.find({ order: { id: "ASC" } });
    console.log(roles);
  })
  .catch((error) => console.log(error));
